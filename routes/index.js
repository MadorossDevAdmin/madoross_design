var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', null);
});

router.get('/main', function(req, res, next) {
  res.render('layout', { view: 'main/MAIN' });
});

router.get('/product/list', function(req, res, next) {
  res.render('layout', { view: 'product/LIST_00' });
});

router.get('/product/detail', function(req, res, next) {
  res.render('layout', { view: 'product/DETAIL_00' });
});

router.get('/product/reservation', function(req, res, next) {
  res.render('layout', { view: 'product/RESERVATION_00' });
});

module.exports = router;
