var express   = require('express')
    ,path     = require('path')
    ,favicon  = require('serve-favicon')
    ,logger   = require('morgan')
    ,cookieParser = require('cookie-parser')
    ,bodyParser   = require('body-parser')
    ,lessMiddleware = require('less-middleware');

var app = express();

var LISTEN_PORT = (process.env.PORT) ? process.env.PORT : 1339;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(lessMiddleware(
  path.join(__dirname, 'less'),
  {
  	dest: path.join(__dirname, 'public')
  }
));
app.use(express.static(path.join(__dirname, 'public')));


var index = require('./routes/index')
var users = require('./routes/users')

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(LISTEN_PORT);

module.exports = app;
