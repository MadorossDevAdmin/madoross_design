$(function(){
  setMapArea();
});


function setMapArea(){
  var mapContainer = document.getElementById('mapContainer'), // 지도를 표시할 div
    lat = mapContainer.getAttribute('data-lat'),
    lon = mapContainer.getAttribute('data-lon'),
    mapOption = {
        center: new daum.maps.LatLng(lat, lon), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };

  var mapWidth = mapContainer.offsetWidth;
  var mapHeight = mapContainer.offsetHeight;

  var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
  var markerPosition  = new daum.maps.LatLng(lat, lon);
  var marker = new daum.maps.Marker({
      position: markerPosition
  });

  mapContainer.style.width = mapWidth;
  mapContainer.style.height = mapHeight;
  map.relayout();
  marker.setMap(map);
  map.setCenter(markerPosition);
  map.setDraggable(false);
  map.setZoomable(false);

  var mapTypeControl = new daum.maps.MapTypeControl();

  // 지도에 컨트롤을 추가해야 지도위에 표시됩니다
  // daum.maps.ControlPosition은 컨트롤이 표시될 위치를 정의하는데 TOPRIGHT는 오른쪽 위를 의미합니다
  map.addControl(mapTypeControl, daum.maps.ControlPosition.TOPRIGHT);

  // 지도 확대 축소를 제어할 수 있는  줌 컨트롤을 생성합니다
  var zoomControl = new daum.maps.ZoomControl();
  map.addControl(zoomControl, daum.maps.ControlPosition.RIGHT);

}
